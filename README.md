[LINK TO DOCUMENTATION](https://gitlab.com/benjamin_agic/BankApp/-/wikis/home)

# Django Bank

Banking system developed for school purposes. Requirements given by educational institution KEA.

### Tables:

-   user
-   userProfile (for storing phone number)
    -   Has 1 to 1 relationship with users
-   accounts
-   transactions (1 transaction should be 2 rows)

### Debit and credit

-   one account gets money removed
-   the other account gets money added

### Additional info:

Loan is an account with a negative balance.
Account balance is calculated by summing up values from transactions.

# To do:

-   [x] A customer can have any number of bank accounts

-   [x] For future multi-factor authentication, we must record the customer’s telephone number

-   [ ] The bank ranks its customers into three groups: basic, silver, and gold - the system must keep track of this information

-   [ ] Customers ranked silver and gold can loan money from the bank

-   [x] Customers can make payments on their loans

-   [x] Customers can transfer money from their accounts

-   [ ] if the account balance is sufficient

-   [x] Customers can view their accounts, accounts movements, and accounts balance

-   [x] Bank employees can view all customers and accounts

-   [x] Bank employees can create new customers and accounts and change customer rank

-   [x] Use Python 3.7 or newer, Django 3.0 or newer

-   [x] Your project should be [documented](https://gitlab.com/benjamin_agic/BankApp/-/wikis/home) appropriately (e.g., ER-Diagram, short intro), but you are not supposed to write a report.

## Workers

Run scheduler:

```
python manage.py rqscheduler --interval 60
```

Run worker:

```
python manage.py rqworker
```
