from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from djmoney.money import Money
from django.contrib.auth.models import User
from .models import Account, Transaction
from django.http import HttpResponseRedirect
import django_rq
from django.db.models import Q
from bank_accounts.jobs import create_transaction
from operator import attrgetter
from math import ceil
from django.contrib import messages


# @login_required(login_url='/users/login/')
def accounts_index(request):
    accounts = Account.get_accounts(request.user)
    accounts_count = Account.get_accounts(request.user).count()
    context = {
        "accounts_count":accounts_count,
        "usersAccounts": Account.get_accounts(request.user)
    }
    query = ""

    
    if request.GET:
        query = request.GET['q']
        context['query'] = str(query)

    accounts = sorted(get_Bankaccount_search(request, query), key=attrgetter('name'), reverse=True)
    context['accounts'] = accounts
    return render(request, 'bank_accounts/accounts/index.html', context )

def loan_index(request):
    loans = Account.get_loans(request.user)
    
    context = {
        "loans" : loans,
    }
    
    
    return render(request, 'bank_accounts/accounts/loans.html', context)


# @login_required(login_url='/users/login/')
def accounts_show(request, id):
    account = Account.objects.get(id=id)
   
    return render(request, 'bank_accounts/accounts/show.html', {
        'account': account,
        'balance': account.get_balance(),
        'transactions': account.transaction_set.all()
    })

# @login_required(login_url='/users/login/')
def create_account(request):
    context = {
        'users': User.objects.all()
    }

    if request.method == "POST":
      account = Account()
      name = request.POST['name']
      account.user = request.user
      account.name = name
      account.save()

      return HttpResponseRedirect(reverse('bank_accounts:accounts_index'))

    
    return render(request, 'bank_accounts/accounts/createAccount.html', context)

# @login_required(login_url='/users/login/')
def create_savings(request):
    accounts = Account.get_savings(request.user).count()
    context = {}

    if request.method == "POST":
        account = Account()
        name = request.POST['name']
        account.user = request.user
        account.name = name
        account.accountType = 'savings'
        account.save()

        return HttpResponseRedirect(reverse('bank_accounts:accounts_index'))

    
    return render(request, 'bank_accounts/accounts/createSavings.html', context)

def delete_account(request, id):
    pass


def get_Bankaccount_search(request, query=None):
    queryset = []
    queries = query.split(" ")
    for q in queries:
        bankAccountObj = Account.objects.filter(Q(name__icontains=q)).distinct()
        for account in bankAccountObj:
            queryset.append(account)
    return list(set(queryset))


# @login_required(login_url='/users/login/')
def transactions_create(request):
    return render(request, 'bank_accounts/transactions/create.html', {
        'accounts': Account.get_accounts(request.user),
        'allAccounts' : Account.objects.all()
    })

# @login_required(login_url='/users/login/')
def transactions_store(request):
    if not request.method == 'POST':
        return 

    errors = []

    if not request.POST.get('from'):
        errors.append('From must be set')

    if not request.POST.get('to'):
        errors.append('To must be set')

    if not request.POST.get('amount'):
        errors.append('Amount must be set')

    if len(errors) > 0:
        for error in errors:
            messages.error(request, error)

        return render(request, 'bank_accounts/transactions/create.html')
    
    amount = Money(request.POST.get('amount'), 'USD')
    from_account = Account.objects.get(id=request.POST.get('from'))
    to_account = Account.objects.get(id=request.POST.get('to'))
    area = 'Transfer'

    if not from_account.user.id == request.user.id:
        errors.append('You can only transfer money from your own account')

    if amount < Money(0, 'USD'):
        errors.append('Amount must be more than 0')

    if from_account.get_balance() < amount:
        errors.append('You don\'t have enough funds')

    if len(errors) > 0:
        for error in errors:
            messages.error(request, error)

        return render(request, 'bank_accounts/transactions/create.html')

    recipient_savings_accounts = Account.get_savings(to_account.user)

    if len(recipient_savings_accounts) > 0:
        if amount > Money(0, 'USD'):
            savings_percentage = 0.02 # Save 2%
            savings_amount_per_account = amount * savings_percentage
            total_savings_amount = len(recipient_savings_accounts) * savings_amount_per_account
            amount = amount - total_savings_amount

            for account in recipient_savings_accounts:
                request.user.userprofile.deposit_savings(from_account, account, savings_amount_per_account, area)

    create_transaction(from_account, to_account, amount, area)

    return redirect('bank_accounts:accounts_index')



def create_loan(request):
    context = {
        "accounts": Account.get_accounts(request.user)
    }

    if request.method == 'POST':
        amount = Money(request.POST.get('amount'), 'USD')
        name = request.POST['name']
        to_account = Account.objects.get(id=request.POST.get('to'))
        if request.user.userprofile.loan_check:
            if to_account.get_balance() > Money(0, 'USD'):
                return render(request, 'bank_accounts/transactions/createLoan.html', {
                'error': 'Your account has funds on it already!'
            })
            else:
                request.user.userprofile.make_loan(amount,to_account ,name)
        else:
            messages.error(request, 'You don\'t have the correct rank')
        return HttpResponseRedirect(reverse('bank_accounts:accounts_index'))
    else:

        return render(request, 'bank_accounts/transactions/createLoan.html', context)

def pay_loan(request):
    context = {
        "accounts": Account.get_accounts(request.user),
        "loans": Account.get_loans(request.user)
    }
    if request.method == 'POST':
        from_account = Account.objects.get(id=request.POST.get('from'))
        to_loan = Account.objects.get(id=request.POST.get('to'))
        amount = to_loan.get_balance()
        area = 'Loan payback'
        if to_loan.get_balance() > Money(0, 'USD'):
                return render(request, 'bank_accounts/transactions/payLoan.html', {
                'error': 'Balance is not negative on loan!'
            })
        else:
            request.user.userprofile.pay_loan(amount, from_account, to_loan, area)
        return HttpResponseRedirect(reverse('bank_accounts:accounts_index'))
    else:

        return render(request, 'bank_accounts/transactions/payLoan.html', context)
