from django.db import models
from djmoney.money import Money
from djmoney.models.fields import MoneyField
from django.contrib.auth.models import User


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(default = 'Main account', max_length=250)
    accountType = models.CharField(default='checking', max_length=250)
    
    @classmethod
    def find_by_id(cls, id):
        return cls.objects.get(pk=id)

    @classmethod
    def get_accounts(cls, user):
        return cls.objects \
            .annotate(balance=models.functions.Coalesce(
                models.Sum('transaction__amount'), 0)
            ) \
            .filter(user=user) \
            .filter(balance__gte=0)

    @classmethod
    def get_loans(cls, user):
        return cls.objects \
            .annotate(balance=models.Sum('transaction__amount')) \
            .filter(user=user) \
            .filter(balance__lt=0)

    
    @classmethod
    def get_savings(cls, user):
        return cls.objects \
            .annotate(balance=models.Sum('transaction__amount')) \
            .filter(user=user) \
            .filter(accountType='savings') 
            


    def get_balance(self):
        balance = self.transaction_set \
            .aggregate(models.Sum('amount')) \
            .get('amount__sum') or 0
        return Money(balance, 'USD')

    def __str__(self):
        return f'{self.name} Account'

class Transaction(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = MoneyField(
        max_digits=14,
        decimal_places=2,
        default_currency='USD'
    )
    interacting_account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name='interacting_account',
        null=True
    )
    area = models.CharField(default='transfer', max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, from_account, to_account, amount, area):
        from_transaction = cls()
        from_transaction.account_id = from_account.id
        from_transaction.interacting_account_id = to_account.id
        from_transaction.amount = -amount
        from_transaction.area = area
        from_transaction.save()

        to_transaction = cls()
        to_transaction.account_id = to_account.id
        to_transaction.interacting_account_id = from_account.id
        to_transaction.amount = amount
        to_transaction.area = area
        to_transaction.save()

    @classmethod
    def create_to_bank(cls, account, amount, area):
        transaction = cls()

        transaction.account_id = account.id
        transaction.amount = -amount
        transaction.area = area

        transaction.save()

        return transaction

    @classmethod
    def create_from_bank(cls, account, amount, area):
        transaction = cls()

        transaction.account_id = account.id
        transaction.amount = amount
        transaction.area = area

        transaction.save()

        return transaction

