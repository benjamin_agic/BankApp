from django.urls import path

from . import views



app_name = "bank_accounts"

urlpatterns = [
   path('', views.accounts_index, name='accounts_index'),
   path('loans/', views.loan_index, name='loan_index'),
   path('create_account/', views.create_account, name='create_account'),
   path('create_savings/', views.create_savings, name='create_savings'),
   path('account/<int:id>', views.accounts_show, name='accounts_show'),
   path('transactions/create', views.transactions_create, name='transactions_create'),
   path('transactions/createLoan', views.create_loan, name='create_loan'),
   path('transactions/payLoan', views.pay_loan, name='pay_loan'),
   path('transactions', views.transactions_store, name='transactions_store'),

]
