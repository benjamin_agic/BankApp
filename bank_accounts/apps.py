from django.apps import AppConfig


app_name = 'bank_accounts'

class BankAccountsConfig(AppConfig):
    name = 'bank_accounts'
