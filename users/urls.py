from django.urls import path

from . import views

app_name='users'

urlpatterns = [
    path('', views.users, name='users'),
    path('<int:id>/', views.user, name='user'),
    path('<int:id>/profile', views.user_profile, name='user_profile'),
    path('home/', views.home, name='home'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
]

