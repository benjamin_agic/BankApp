from django.urls import path
from . import api

app_name='users_api'

urlpatterns = [
    path('all', api.UsersListView.as_view(), name='all'),
    path('<int:id>', api.UsersUpdateView.as_view(), name='update'),
]
