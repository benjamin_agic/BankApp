from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

        email = serializers.EmailField()
        username = serializers.CharField(max_length=200)

        fields = ['id', 'username', 'email']
