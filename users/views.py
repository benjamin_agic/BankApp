from django.shortcuts import render, reverse, get_object_or_404, redirect
from .models import User, UserProfile, UserRank
from bank_accounts.models import Account
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.http import HttpResponseRedirect
from django.db.models import Q
from djmoney.money import Money
from django.contrib import messages


def users(request):
    # if not request.user.is_superuser:
    #     return HttpResponseRedirect(reverse('users:home'))

    return render(request, 'users/users.html')

def profile(request):
    context = {
        'users': User.objects.filter(pk=request.user.id),
        'accounts': Account.get_savings(request.user)
    }

    return render(request, 'users/profile.html',context)

def user(request, id):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse('users:home'))

    customer = get_object_or_404(User, pk=id)
    context = {
        'accounts': Account.objects.filter(user_id=id).all(),
        "userRanks": UserRank.objects.all(),
        "customer": customer
    }
    if request.method == "POST":
        userRank = request.POST['userRank']
        customer.userRank = get_object_or_404(UserRank, id=userRank)
        customer.save()
        return HttpResponseRedirect(reverse('users:home'))
    return render(request, 'users/user.html', context)

def user_profile(request, id):
    if not request.method == "POST":
        return HttpResponseRedirect(reverse('users:home'))

    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse('users:home'))

    user = User.objects.get(pk=id)
    rankId = request.POST.get('rank')
    rank = UserRank.find_by_id(rankId)

    user.userprofile.userRank = rank
    user.userprofile.save()

    messages.success(request, 'Rank updated')

    return HttpResponseRedirect(reverse('users:users'))


def home(request):
    context = {
        'users': User.objects.all()
    }

    return render(request, 'users/home.html', context)


def login(request):
    contex={}

    if request.method == 'POST':
        user = authenticate(request, username= request.POST['username'], password = request.POST['password'] )
        if user:
            dj_login(request, user)
            return HttpResponseRedirect(reverse('users:home'))
        else:
            contex={'error':'Bad username or password'}
    return render(request,'users/login.html',contex)


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('users:login'))

def register(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse('users:home'))

    context = {
       "userRanks": UserRank.objects.all()
    }

    if request.method == "POST":
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        user_name = request.POST['username']
        email = request.POST['email']
        phone = request.POST['phone']
        rank = request.POST['rank']

        if password == confirm_password:
            user = UserProfile.create_customer(user_name, password, email, phone, rank)
            if user:
                return HttpResponseRedirect(reverse('users:users'))
            else:
                context = {
                    'error': 'Could not create user account - please try again.'
                }
        else:
            context = {
                'error': 'Passwords did not match. Please try again.',
            }
    return render(request, 'users/create_user.html', context)



