from .serializers import UserSerializer
from rest_framework import generics, status, permissions
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from rest_framework.renderers import JSONRenderer
from django.contrib.auth.models import User

class IsUserOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user.is_superuser:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.owner == request.user

class UsersListView(generics.ListAPIView):
    def get(self, request):
        query = request.GET.get('query')
        serializer = UserSerializer(User.objects.filter(username__contains=query), many=True)
        
        return Response(serializer.data)

class UsersUpdateView(generics.UpdateAPIView):
    def patch(self, request, id):
        user = User.objects.get(pk=id)
        serializer = UserSerializer(user, data=request.data)
        permission_classes = [IsUserOrAdmin]

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)

        serializer.save()
        
        return Response(serializer.data)
