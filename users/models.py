from django.db import models
from bank_accounts.models import Account, Transaction
import random
from django.conf import settings
from django.contrib.auth.models import User
import django_rq
from django.dispatch import receiver
from django.db.models.signals import post_save

from bank_accounts.jobs import create_transaction


class UserRank(models.Model):
    name = models.CharField(max_length=35, unique=True)
    value = models.IntegerField(unique=True)

    @classmethod
    def find_by_id(cls, id):
        return cls.objects.get(pk=id)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phoneNumber = models.CharField(default='52525', max_length=10)
    userRank = models.ForeignKey(UserRank, on_delete=models.PROTECT)  
    
    @classmethod
    def create_customer(cls, username, password, email, phone, rankId) -> User:
        user = User.objects.create_user(username=username, password=password, email=email)
        rank = UserRank.find_by_id(rankId)
        print(f"@@@@@@@@@@ {rankId}")
        print(f"!!!!!!!!!! {rank}")

        profile = cls()

        profile.user = user
        profile.userRank = rank
        profile.phoneNumber = phone

        profile.save()

        return user

    @property
    def loan_check(self) -> bool:
        return self.userRank.value >= settings.CUSTOMER_RANK_LOAN
    
    @property
    def did_payback(self) -> bool:
        return self.userRank.value >= settings.CUSTOMER_RANK_LOAN
    
    def make_loan(self, amount, to_account, name):
        assert self.loan_check, 'User membership rank not high enough!'
        loan = Account(user=self.user, name=f'{name}')
        loan.save()
        create_transaction(loan, to_account, amount, 'loan')

    def pay_loan(self, amount, from_account, to_loan, area): 
        create_transaction(to_loan, from_account, amount, area)

    def deposit_savings(self, from_account, savings_account, amount, area):
        create_transaction(from_account, savings_account, amount, area)
    
    def __str__(self):
        return f'{self.user.username} UserProfile'
