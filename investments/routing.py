from django.urls import re_path

from .consumers import PriceConsumer, InvestmentConsumer

websocket_urlpatterns = [
    re_path(r'ws/prices/(?P<ticker>\w+)/$', PriceConsumer.as_asgi()),
    re_path(r'ws/investments/(?P<ticker>\w+)/$', InvestmentConsumer.as_asgi()),
]
