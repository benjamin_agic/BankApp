import requests
import channels.layers
from collections import defaultdict
from django_rq import job
from bankApp import settings
from asgiref.sync import async_to_sync

channel_layer = channels.layers.get_channel_layer()

def fetch_stock_prices():
    from .models import Price, Stock
    from .consumers import money_to_string

    symbols = ['AAPL', 'INTC', 'MSFT', 'PS']

    updates = []

    for symbol in symbols:
        params = {
            'function': 'TIME_SERIES_INTRADAY',
            'symbol': symbol,
            'interval': '1min',
            'apikey': settings.STOCK_API_APIKEY
        }
        result = requests.get(settings.STOCK_API_URL, params=params)
        data = result.json().get('Time Series (1min)')

        if data == None:
            return

        latest_result = next(iter(data.values()))
        price = latest_result.get('4. close')

        stock = Stock.find_by_ticker(symbol)
        stock.record_price(price)

        update = { 
            symbol.lower(): {
                'price': money_to_string(stock.price.formatted),
                'difference': money_to_string(stock.price.difference)
            } 
        }

        updates.append(update)

        async_to_sync(channel_layer.group_send)(
            f"price_{symbol.lower()}",
            {
                'type': 'price_update',
                'tickers': update
            }
        )

    tickers = {}
    for update in updates:
        tickers.update(update)

    async_to_sync(channel_layer.group_send)(
        'price_all',
        {
            'type': 'price_update',
            'tickers': tickers
        }
    )
