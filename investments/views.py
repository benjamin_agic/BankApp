from django.shortcuts import render, redirect
from bank_accounts.models import Account
from .models import Stock, Investment
from . import forms
from django.views.generic import TemplateView
from django.contrib import messages
from djmoney.money import Money
from bank_accounts.models import Account, Transaction


def dashboard(request):
    context = {
        'stocks': Stock.get_all(),
        'stocks_with_investments': Stock.get_all_with_investments(request.user),
    }

    return render(request, 'investments/dashboard.html', context)

def stock(request, ticker):
    stock = Stock.find_by_ticker(ticker)

    context = {
        'stock': stock,
        'history': stock.get_history(),
        'investments': stock.get_investments(request.user),
        'total_shares': stock.get_total_shares(request.user),
        'total_value': stock.get_total_value(request.user)
    }

    return render(request, 'investments/stock.html', context)

class BuyView(TemplateView):
    def get(self, request, ticker):
        stock = Stock.find_by_ticker(ticker)

        context = {
            'form': forms.BuyStockForm(user=request.user),
            'stock': stock,
            'accounts': Account.get_accounts(request.user),
        }

        return render(request, 'investments/buy.html', context)

    def post(self, request, ticker):
        stock = Stock.find_by_ticker(ticker)
        form = forms.BuyStockForm(request.POST, user=request.user)

        if not form.is_valid():
            for key in form.errors:
                for error in form.errors[key]:
                    messages.error(request, f"{key}: {error}")
            return redirect('investments:buy', ticker=stock.ticker)

        data = form.cleaned_data
        account = Account.find_by_id(data.get('account'))
        amount = data.get('amount')

        Transaction.create_to_bank(account, amount, 'investment')
        Investment.create(
            request.user,
            stock,
            amount,   
        )
        messages.success(request, f"{stock.name} was purchased")

        return redirect('investments:dashboard')

class SellView(TemplateView):
    def get(self, request, ticker, investment_id):
        stock = Stock.find_by_ticker(ticker)
        investment = Investment.find_by_id(investment_id)

        context = {
            'form': forms.SellStockForm(user=request.user),
            'stock': stock,
            'investment': investment,
            'accounts': Account.get_accounts(request.user),
        }

        return render(request, 'investments/sell.html', context)

    def post(self, request, ticker, investment_id):
        stock = Stock.find_by_ticker(ticker)
        investment = Investment.find_by_id(investment_id)

        form = forms.SellStockForm(request.POST, user=request.user)

        if not form.is_valid():
            for key in form.errors:
                for error in form.errors[key]:
                    messages.error(request, f"{key}: {error}")
            return redirect('investments:buy', ticker=stock.ticker, investment_id=investment_id)

        data = form.cleaned_data
        account = Account.find_by_id(data.get('account'))
        amount = Money(data.get('amount'), 'USD')

        if amount > investment.value:
            amount = investment.value
            investment.delete()
        else:
            investment.sell(amount)

        Transaction.create_from_bank(account, amount, 'investment')
        messages.success(request, f"{stock.name} was sold")

        return redirect('investments:dashboard')

class SellAllView(TemplateView):
    def get(self, request, ticker):
        stock = Stock.find_by_ticker(ticker)

        context = {
            'form': forms.SellAllStockForm(user=request.user),
            'stock': stock,
            'accounts': Account.get_accounts(request.user),
            'total_value': stock.get_total_value(request.user),
        }

        return render(request, 'investments/sell-all.html', context)

    def post(self, request, ticker):
        stock = Stock.find_by_ticker(ticker)

        form = forms.SellAllStockForm(request.POST, user=request.user)

        if not form.is_valid():
            for key in form.errors:
                for error in form.errors[key]:
                    messages.error(request, f"{key}: {error}")
            return redirect('investments:sell_all', ticker=stock.ticker)

        data = form.cleaned_data
        account = Account.find_by_id(data.get('account'))
        investments = stock.get_investments(request.user)

        Transaction.create_from_bank(
            account,
            stock.get_total_value(request.user),
            'investment'
        )

        for investment in investments:
            investment.delete()

        messages.success(request, f"{stock.name} was sold")

        return redirect('investments:dashboard')

