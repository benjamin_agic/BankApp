from django import forms
from bank_accounts.models import Account

class StockForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        accounts = Account.get_accounts(self.user).values_list('id', 'name')

        super(StockForm, self).__init__(*args, **kwargs)

        self.fields['account'].choices = [(None, 'Select')] + list(accounts)

class BuyStockForm(StockForm):
    account = forms.ChoiceField(
        label='Account',
        required=True,
        choices=()
    )
    amount = forms.IntegerField(
        label='Amount in dollars',
        required=True,
        min_value=1
    )


class SellStockForm(StockForm):
    account = forms.ChoiceField(
        label='Account',
        required=True,
        choices=()
    )
    amount = forms.FloatField(
        label='Amount in dollars',
        required=True,
        min_value=1
    )


class SellAllStockForm(StockForm):
    account = forms.ChoiceField(
        label='Account',
        required=True,
        choices=()
    )

