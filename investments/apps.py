import django_rq
from datetime import datetime
from django.apps import AppConfig
from .jobs import fetch_stock_prices


class InvestmentsConfig(AppConfig):
    name = 'investments'

    def ready(self):
        scheduler = django_rq.get_scheduler('default', interval=2)

        # Delete any existing jobs in the scheduler when the app starts up
        for job in scheduler.get_jobs():
            job.delete()

        scheduler.schedule(
            scheduled_time=datetime.utcnow(), # Time for first execution, in UTC timezone
            func=fetch_stock_prices, 
            interval=60, # Time in seconds before it repeats
        )

