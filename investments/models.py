from django.db import models
from djmoney.money import Money
from django.db.models import Sum
from djmoney.models.fields import MoneyField
from django.contrib.auth.models import User

class Stock(models.Model):
    name = models.CharField(max_length=250)
    ticker = models.CharField(max_length=250)

    @classmethod
    def find_by_ticker(cls, ticker):
        return cls.objects.get(ticker=ticker.upper())

    @classmethod
    def get_all(cls):
        return cls.objects.all()

    @classmethod
    def get_all_with_investments(cls, user):
        stocks = cls.get_all()
        stock_investments = []

        for stock in stocks:
            if stock.has_investment(user):
                stock.total_value = stock.get_total_value(user)
                stock.total_shares = stock.get_total_shares(user)
                stock_investments.append(stock)


        return stock_investments

    def record_price(self, price_as_string):
        return Price.create(self, Money(price_as_string, 'USD'))

    def get_history(self):
        return Price.get_history_by_stock(self)[:10]

    def has_investment(self, user):
        return bool(Investment.find_by_stock_and_user(self, user).count())

    def get_investments(self, user):
        return Investment.find_by_stock_and_user(self, user)

    def get_total_shares(self, user):
        return self.get_investments(user).aggregate(Sum('shares')).get('shares__sum')

    def get_total_value(self, user):
        return sum(investment.value for investment in self.get_investments(user))

    @property
    def price(self):
        return Price.get_latest_price_by_stock(self)

class Price(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    value = MoneyField(
        max_digits=14,
        decimal_places=2,
        default_currency='USD'
    )
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, stock, value):
        price = cls()

        price.stock = stock
        price.value = value

        price.save()

        return price

    @classmethod
    def get_history_by_stock(cls, stock):
        return cls.objects.filter(stock=stock).order_by('-id')

    @classmethod
    def get_latest_price_by_stock(cls, stock):
        return cls.get_history_by_stock(stock).first()

    @classmethod
    def get_price_by_stock_and_time(cls, stock, timestamp):
        return cls.get_history_by_stock(stock).filter(created_at__lt=timestamp).first()

    def get_previous_price(self):
        return Price.objects.filter(stock=self.stock, id__lt=self.id).order_by('id').last()

    @property
    def formatted(self):
        return self.value

    @property
    def difference(self):
        previous_price = self.get_previous_price()

        if not previous_price:
            return Money(0, 'USD')

        return self.value - self.get_previous_price().value


class Investment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    shares = models.DecimalField(max_digits=19, decimal_places=10)
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, user, stock, amount):
        investment = cls()

        investment.user = user
        investment.stock = stock

        shares = amount / stock.price.value.amount
        investment.shares = shares

        investment.save()

        return investment

    @classmethod
    def find_by_id(cls, id):
        return cls.objects.get(pk=id)

    @classmethod
    def find_by_user(cls, user):
        return cls.objects.filter(user=user)

    @classmethod
    def find_by_stock(cls, stock):
        return cls.objects.filter(stock=stock)

    @classmethod
    def find_by_stock_and_user(cls, stock, user):
        return cls.objects.filter(stock=stock).filter(user=user)

    def sell(self, amount):
        shares = amount / self.stock.price.value

        self.shares = self.shares - shares

        self.save()

    @property
    def value(self):
        return self.shares * self.stock.price.value

    @property
    def purchase_price(self):
        price = Price.get_price_by_stock_and_time(self.stock, self.created_at)

        return price.formatted

    @property
    def purchase_value(self):
        return self.shares * self.purchase_price
    
