from django.urls import path

from . import views


app_name = "investments"

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('stocks/<str:ticker>', views.stock, name='stock'),
    path('stocks/<str:ticker>/buy', views.BuyView.as_view(), name='buy'),
    path('stocks/<str:ticker>/sell-all', views.SellAllView.as_view(), name='sell_all'),
    path('stocks/<str:ticker>/sell/<int:investment_id>', views.SellView.as_view(), name='sell'),
]
