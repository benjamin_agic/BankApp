import json
from collections import defaultdict
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from .models import Stock, Investment

def money_to_string(money):
    return f"{money}"

class PriceConsumer(WebsocketConsumer):
    def connect(self):
        self.ticker = self.scope['url_route']['kwargs']['ticker']
        self.group_name = 'price_%s' % self.ticker

        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )
        
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def receive(self, text_data):
        pass

    def price_update(self, event):
        tickers = event['tickers']
        self.send(text_data=json.dumps(tickers))
        # async_to_sync(channel_layer.group_send)('price_aapl', {'type': 'price_update', 'ticker': 'aapl', 'price': '$100', 'difference': '$3.14'})
        # async_to_sync(channel_layer.group_send)('price_all', {'type': 'price_update', 'tickers': { 'aapl': { 'price': '$100', 'difference': '$3.14' } }})
        

class InvestmentConsumer(WebsocketConsumer):
    def connect(self):
        self.user = self.scope["user"]
        self.ticker = self.scope['url_route']['kwargs']['ticker']
        
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        data = json.loads(text_data)
        message = data['message']

        if message == 'update':
            stocks = Stock.get_all() if self.ticker == 'all' else [Stock.find_by_ticker(self.ticker)]
            response = defaultdict(list)

            for stock in stocks:
                investments = Investment.find_by_stock_and_user(stock, self.user)

                response['stocks'].append({
                    'ticker': stock.ticker,
                    'value': money_to_string(stock.get_total_value(self.user))
                })

                for investment in investments:
                    response['investments'].append({
                        'id': investment.id,
                        'value': money_to_string(investment.value)
                    })
            
            self.send(text_data=json.dumps(response))

        
