from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def has_investment(context, stock):
    return stock.has_investment(context['user'])
